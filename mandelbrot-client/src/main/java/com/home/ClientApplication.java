package com.home;

import com.home.exception.InvalidDivisorException;
import com.home.exception.InvalidInputException;
import com.home.exception.InvalidNumberException;
import com.home.exception.ServerIpAdressIsMissingOrInvalid;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

@Slf4j
public class ClientApplication {

    private static final String MSG_INPUT_IS_NOT_VALID = "Input is not valid";
    private static final String MSG_DIVISION_SHOULD_BE_SMALLER_THAN_WIDTH_OR_HEIGHT = "Division should be smaller than width or height";
    private static final String MSG_MINIMUM_VALUE_OF_IMAGINARY_PART = "Minimum value of imaginary part of c should be smaller than maximum value";
    private static final String MSG_MINIMUM_VALUE_OF_REAL_PART = "Minimum value of real part of c should be smaller than maximum value";
    private static final String MSG_MSG_POSITIVE_NUMBER_EXCEPTION = "Width, height, division and maxIteration should be positive";
    public static final String MSG_INVALID_SERVER_IP_ADRESS = "Invalid server ip adress";

    public static void main(String[] args) throws IOException, InterruptedException,
            InvalidInputException, InvalidDivisorException, InvalidNumberException, ServerIpAdressIsMissingOrInvalid {

        ClientApplication clientApplication = new ClientApplication();

        CLParamMapper clParamMapper = clientApplication.retrieveCommandLineMapper(args);
        clientApplication.validateCommandLineParams(clParamMapper);

        Client client = new Client();
        Response response = client.distributeWorkloadAcrossTheServers(clParamMapper);
        client.drawImage(clParamMapper.getWidth(), clParamMapper.getHeight(), response, clParamMapper.getMaxIteration());
    }

    /**
     * Retrieves the map of parameters which are taken from command line
     * Parameters:
     * width: column count of whole image
     * height: row count of whole image
     * division: dimension of sub-pictures
     * servers: server addresses
     * maxCRE: maximum value of real part of C
     * maxCIM: maximum value of imaginary part of c
     * minCRE: minimum value of real part of c
     * minCIM: minimum value of imaginary part of c
     * maxIteration: maximum iteration count
     * @return CLParamMapper.class
     * @throws InvalidInputException
     */
    private CLParamMapper retrieveCommandLineMapper(String[] args) throws InvalidInputException {
        CLParamMapper mapper = new CLParamMapper();
        try {
            double minCRE = Double.parseDouble(args[0]);
            mapper.setMinCRE(minCRE);

            double minCIM = Double.parseDouble(args[1]);
            mapper.setMinCIM(minCIM);

            double maxCRE = Double.parseDouble(args[2]);
            mapper.setMaxCRE(maxCRE);

            double maxCIM = Double.parseDouble(args[3]);
            mapper.setMaxCIM(maxCIM);

            int maxIteration = Integer.parseInt(args[4]);
            mapper.setMaxIteration(maxIteration);

            int width = Integer.parseInt(args[5]);
            mapper.setWidth(width);

            int height = Integer.parseInt(args[6]);
            mapper.setHeight(height);

            int division = Integer.parseInt(args[7]);
            mapper.setDivision(division);

            String[] servers = new String[args.length - 1 - 7];
            System.arraycopy(args, 8, servers, 0, args.length - 8);
            mapper.setServers(servers);

        } catch (Exception e) {
            log.error(MSG_INPUT_IS_NOT_VALID);
            throw new InvalidInputException(MSG_INPUT_IS_NOT_VALID);
        }

        return mapper;
    }

    /**
     * Validates the parameters which are taken from command line
     * @param map
     * @throws InvalidDivisorException
     * @throws InvalidNumberException
     */
    void validateCommandLineParams(CLParamMapper map) throws InvalidDivisorException, InvalidNumberException, ServerIpAdressIsMissingOrInvalid {

        int width = map.getWidth();
        int height = map.getHeight();
        int division = map.getDivision();
        int maxIteration = map.getMaxIteration();
        double minCIM = map.getMinCIM();
        double maxCIM = map.getMaxCIM();
        double minCRE = map.getMinCRE();
        double maxCRE = map.getMaxCRE();
        String[] servers = map.getServers();


        Pattern p = Pattern.compile("^"
                + "(((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}" // Domain name
                + "|"
                + "localhost" // localhost
                + "|"
                + "(([0-9]{1,3}\\.){3})[0-9]{1,3})" // Ip
                + ":"
                + "[0-9]{1,5}$"); // Port

        if (division > width || division > height) {
            log.error(MSG_DIVISION_SHOULD_BE_SMALLER_THAN_WIDTH_OR_HEIGHT);
            throw new InvalidDivisorException(MSG_DIVISION_SHOULD_BE_SMALLER_THAN_WIDTH_OR_HEIGHT);
        } else if (maxCIM <= minCIM) {
            log.error(MSG_MINIMUM_VALUE_OF_IMAGINARY_PART);
            throw new InvalidNumberException(MSG_MINIMUM_VALUE_OF_IMAGINARY_PART);
        } else if (maxCRE <= minCRE) {
            log.error(MSG_MINIMUM_VALUE_OF_REAL_PART);
            throw new InvalidNumberException(MSG_MINIMUM_VALUE_OF_REAL_PART);
        } else if (width < 0 || height < 0 || division < 0 || maxIteration < 0) {
            log.error(MSG_MSG_POSITIVE_NUMBER_EXCEPTION);
            throw new InvalidNumberException(MSG_MSG_POSITIVE_NUMBER_EXCEPTION);
        } else if (servers.length == 0 || Arrays.stream(servers).anyMatch(t -> !p.matcher(t).matches())) {
            log.error(MSG_INVALID_SERVER_IP_ADRESS);
            throw new ServerIpAdressIsMissingOrInvalid(MSG_INVALID_SERVER_IP_ADRESS);
        }

    }
}
