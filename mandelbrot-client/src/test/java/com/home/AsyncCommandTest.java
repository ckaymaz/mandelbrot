package com.home;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
public class AsyncCommandTest {

    private AsyncCommand asyncCommand = new AsyncCommand("127.0.0.1", 22222, null);

    @Before
    public void setUp() {
        try {
            (new Thread(() -> {
                try (ServerSocket server = new ServerSocket(33333); Socket socket = server.accept()) {

                    DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                    String healthCheck = inputStream.readLine();
                    System.out.println(healthCheck);

                    DataOutputStream bw = new DataOutputStream(socket.getOutputStream());

                    bw.write("pong".getBytes());
                    bw.flush();
                    bw.close();
                } catch (Exception ex) {
                    log.error("Health Check is unsuccessful");
                }
            })).start();
        } catch (Exception e) {
            log.error("Health Check is unsuccessful");
        }

    }

    @Test
    public void healthCheckTest() {
        String response = asyncCommand.healthCheck("127.0.0.1", 33333, 1000);
        Assert.assertEquals("pong", response);
    }


}
