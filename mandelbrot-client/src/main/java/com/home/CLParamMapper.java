package com.home;

import lombok.Data;

@Data
class CLParamMapper {

    private int width;
    private int height;
    private int division;
    private String[] servers;
    private double maxCRE;
    private double maxCIM;
    private double minCRE;
    private double minCIM;
    private int maxIteration;
}