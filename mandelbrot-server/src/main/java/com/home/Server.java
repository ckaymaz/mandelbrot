package com.home;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
class Server {

    /**
     * Processes a request coming from client based on the certain parameters
     * @param port
     * @throws IOException
     */
    void processMandelbrotSet(int port) throws IOException {
        Socket socket = null;
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            while (true) {
                try {
                    socket = serverSocket.accept();
                    DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                    DataOutputStream bw = new DataOutputStream(socket.getOutputStream());

                    String commands = inputStream.readLine();

                    log.info(commands);

                    if (commands.equals("exit")) {
                        break;
                    } else if (commands.equals("ping")) {
                        bw.write("pong".getBytes());
                        bw.flush();
                        bw.close();
                    } else {
                        String[] command = commands.split(" ");
                        if (command[0].equals("GET")) {
                            String[] commandParams = command[1].split("/");

                            double minCRE = Double.parseDouble(commandParams[2]);
                            double minCIM = Double.parseDouble(commandParams[3]);
                            double maxCRE = Double.parseDouble(commandParams[4]);
                            double maxCIM = Double.parseDouble(commandParams[5]);
                            int width = Integer.parseInt(commandParams[6]);
                            int height = Integer.parseInt(commandParams[7]);
                            int startX = Integer.parseInt(commandParams[8]);
                            int startY = Integer.parseInt(commandParams[9]);
                            int maxIteration = Integer.parseInt(commandParams[10]);
                            int division = Integer.parseInt(commandParams[11]);

                            int[][] result = calculateFragment(width, height, minCRE, minCIM, maxCRE, maxCIM,
                                    maxIteration, division, startX, startY);

                            bw.write(SerializationUtils.serialize(result));
                        }

                        bw.flush();
                        bw.close();
                    }
                } catch (Exception ex) {
                    log.error("Internal server error {} ", ex.getMessage());
                }

            }
        } catch (Exception ex) {
            log.error("Internal server error {} ", ex.getMessage());
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    /**
     * Calculates a mandelbrot fragment based on the certain parameters
     * @param imageWidth
     * @param imageHeight
     * @param minCRE
     * @param minCIM
     * @param maxCRE
     * @param maxCIM
     * @param maxIteration
     * @param division
     * @param startX
     * @param startY
     * @return
     */
    @SuppressWarnings({"squid:S00107", "squid:S00117"})
    private int[][] calculateFragment(int imageWidth, int imageHeight, double minCRE, double minCIM, double maxCRE,
                                      double maxCIM, int maxIteration, int division, int startX, int startY) {

        int[][] fragment = new int[division][division];

        double reFactor = (maxCRE - minCRE) / ((double) (imageWidth - 1));
        double imFactor = (maxCIM - minCIM) / ((double) (imageHeight - 1));

        int indexX = 0;
        int indexY = 0;

        for (int i = startX; i < startX + division; i++) {
            for (int j = startY; j < startY + division; j++) {

                double cr = minCRE + i * reFactor;
                double ci = maxCIM - j * imFactor;

                double zr = cr;
                double zi = ci;

                int k = 0;
                while (k < maxIteration && zr * zr + zi * zi < 4.0) {

                    double newR = cr + zr * zr - zi * zi;
                    double newI = ci + 2 * zr * zi;

                    zr = newR;
                    zi = newI;

                    k++;
                }

                fragment[indexX][indexY] = k;
                indexY++;
            }
            indexX++;
            indexY = 0;
        }
        return fragment;
    }

}
