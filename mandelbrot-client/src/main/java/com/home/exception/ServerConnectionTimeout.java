package com.home.exception;

public class ServerConnectionTimeout extends RuntimeException {

    public ServerConnectionTimeout(String message) {
        super(message);
    }
}
