package com.home;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

class Client {

    /**
     * Distributes the workload across the server,
     * Collects the iteration counts per (x,y) position from the servers
     * @param map
     * @return Response.class
     * @throws InterruptedException
     */
    Response distributeWorkloadAcrossTheServers(CLParamMapper map) throws InterruptedException {
        int width = map.getWidth();
        int height = map.getHeight();
        int division = map.getDivision();
        String[] servers = map.getServers();
        double maxCRE = map.getMaxCRE();
        double maxCIM = map.getMaxCIM();
        double minCRE = map.getMinCRE();
        double minCIM = map.getMinCIM();
        int maxIteration = map.getMaxIteration();

        int[][] imgData = new int[width][height];
        Response response = new Response(imgData);

        int serverIndex = 0;

        for (int i = 0; i < width - division + 1; i = i + division) {
            for (int j = 0; j < height - division + 1; j = j + division) {
                String serverStr = servers[serverIndex % servers.length];
                int delimiterIndex = serverStr.lastIndexOf(":");
                String serverIp = serverStr.substring(0, delimiterIndex);
                int port = Integer.parseInt(serverStr.substring(delimiterIndex + 1, serverStr.length()));

                AsyncCommand asyncCommand = new AsyncCommand(serverIp, port, response);

                String healthCheck = asyncCommand.healthCheck(serverIp, port, 1000);
                if (healthCheck != null && healthCheck.equals("pong")) {
                    asyncCommand.initParams(minCRE, minCIM, maxCRE, maxCIM, maxIteration, width, height, division, i, j);
                    asyncCommand.start();
                    asyncCommand.join();
                } else {
                    j = j - division; // if server is down, send to another server
                }
                serverIndex++;
            }
        }

        return response;

    }

    /**
     * Draws image
     * @param width
     * @param height
     * @param response
     * @param maxIterations
     * @throws IOException
     */
    void drawImage(int width, int height, Response response, int maxIterations) throws IOException {
        BufferedImage img = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                int h = response.getImageData()[i][j];

                float colorLevel;

                //Specifies density of color per pixel
                if (h < maxIterations) {
                    colorLevel = (float) h / maxIterations;
                } else {
                    colorLevel = 0;
                }
                Color c = new Color(0, colorLevel, 0);  // Green
                img.setRGB(i, j, c.getRGB());

            }
        }
        ImageIO.write(img, "PNG", new File("Mandelbrot.png"));
    }

}