package com.home;

import com.home.exception.InvalidDivisorException;
import com.home.exception.InvalidNumberException;
import com.home.exception.ServerIpAdressIsMissingOrInvalid;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple ClientApplication.
 */
public class ClientApplicationTest
{

    private ClientApplication application = new ClientApplication();

    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldValidateCLParameters() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(100);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(-1);
        mapper.setServers(new String[]{"127.0.0.1:4000"});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = InvalidDivisorException.class)
    public void shouldNotValidateWhenDivisionIsGreaterThanHeightOrWidth() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(10000);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(-1);
        mapper.setServers(new String[]{"127.0.0.1:4000"});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = ServerIpAdressIsMissingOrInvalid.class)
    public void shouldNotValidateWhenServerIpIsMissing() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(100);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(-1);
        mapper.setServers(new String[]{});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = ServerIpAdressIsMissingOrInvalid.class)
    public void shouldNotValidateWhenServerIpIsCorrupt() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(100);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(-1);
        mapper.setServers(new String[]{"1231:a34f"});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = InvalidNumberException.class)
    public void shouldNotValidateWhenMinIsGreaterThanMaxForImaginary() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(100);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(6);
        mapper.setMinCRE(-1);
        mapper.setServers(new String[]{"1231:a34f"});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = InvalidNumberException.class)
    public void shouldNotValidateWhenMinIsGreaterThanMaxForReal() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(1000);
        mapper.setHeight(1000);
        mapper.setDivision(100);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(6);
        mapper.setServers(new String[]{"1231:a34f"});
        mapper.setMaxIteration(1000);

        application.validateCommandLineParams(mapper);
    }

    @Test(expected = InvalidNumberException.class)
    public void shouldNotValidateWhenIntegerParamsLessThanZero() throws InvalidNumberException, InvalidDivisorException, ServerIpAdressIsMissingOrInvalid {
        CLParamMapper mapper = new CLParamMapper();
        mapper.setWidth(-1);
        mapper.setHeight(-1);
        mapper.setDivision(-1);
        mapper.setMaxCIM(2);
        mapper.setMaxCRE(2);
        mapper.setMinCIM(-1);
        mapper.setMinCRE(6);
        mapper.setServers(new String[]{"1231:a34f"});
        mapper.setMaxIteration(-1);

        application.validateCommandLineParams(mapper);
    }

}
