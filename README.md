# Poor Man's Parallelism

The purpose of this application is to render Mandelbrot set based on the parameters given from the command line.
Basically, the application is working as server-client & divide and conquer paradigm.

The tasks of the client module:
* Dividing the given sized grid into given sized sub-grids 
* Spreading the work across to the servers
* Collecting and merging sub-grids
* Drawing the image

The tasks of the server module:
* Getting the work from client based on the parameters in the client request
* Running Mandelbrot set algorithm for each sub-grids

Note that;
* Client sends a health check to the servers just before sending the grid data
  If health check fails it retries to send the data to other servers.
* Output image is created under the mandelbrot-client folder as Mandelbrot.png

## Prerequisites

* Java 1.8
* Maven

## Usage

1. Clone the project from Gitlab.

```
git clone https://gitlab.com/ckaymaz/mandelbrot.git
```


2. Run server application in mandelbrot-server folder with port parameter.

```
cd ${projectfolder}/mandelbrot-server
mvn clean install exec:java -Dexec.mainClass=com.home.ServerApplication -Dexec.args="$(PORT)"

Example :
Server I (localhost:20000)
cd /Users/cigdemkaymaz/IdeaProjects/mandelbrot/mandelbrot-server
mvn clean install exec:java -Dexec.mainClass=com.home.ServerApplication -Dexec.args="20000"

Server II (192.168.1.1:30000)
cd /Users/cigdemkaymaz/IdeaProjects/mandelbrot/mandelbrot-server
mvn clean install exec:java -Dexec.mainClass=com.home.ServerApplication -Dexec.args="30000"
```

3. Run client application in mandelbrot-client folder with parameters.

```
cd ${projectfolder}/mandelbrot-client
mvn clean install exec:java -Dexec.mainClass=com.home.ClientApplication -Dexec.args="$(minCRE) $(minCIM) $(maxCRE) $(maxCIM) $(maxIteration) $(width) $(height) $(division) $(servers)"

Example : 
cd /Users/cigdemkaymaz/IdeaProjects/mandelbrot/mandelbrot-client
mvn clean install exec:java -Dexec.mainClass=com.home.ClientApplication -Dexec.args="-1 -1.5 2 1.5 1024 10000 10000 4 localhost:20000 192.168.1.1:30000"
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management