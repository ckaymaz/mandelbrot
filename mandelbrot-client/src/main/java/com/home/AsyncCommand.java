package com.home;

import com.home.exception.ServerConnectionTimeout;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class AsyncCommand extends Thread {

    public static final String MSG_IMAGE_DATA_COLLECTED = "Image data collected";
    public static final String MSG_MESSAGE_SENT_TO_THE_SERVER = "Message sent to the server";
    public static final String HEATH_CHECK = "Heath Check";
    private String ip;
    private int port;
    private Response response;

    private double minCRE;
    private double minCIM;
    private double maxCRE;
    private double maxCIM;
    private int maxIteration;
    private int width;
    private int height;
    private int division;
    private int startX;
    private int startY;

    AsyncCommand(String ip, int port, Response response) {
        this.ip = ip;
        this.port = port;
        this.response = response;
    }

    @Override
    public void run() {
        try (Socket socket = new Socket(ip, port)) {

            //Send the message to server
            DataOutputStream bw = new DataOutputStream(socket.getOutputStream());
            byte[] arr = createCommand().getBytes();
            bw.write(arr);
            bw.flush();

            log.info(MSG_MESSAGE_SENT_TO_THE_SERVER);

            //Get the return message from the server
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());

            byte[] bytes = IOUtils.toByteArray(inputStream);

            Object result = SerializationUtils.deserialize(bytes);

            int[][] resultData = (int[][]) result;

            //Collect image data
            int indexX = 0;
            int indexY = 0;
            for (int i = startX; i < startX + division; i++) {
                for (int j = startY; j < startY + division; j++) {
                    response.getImageData()[i][j] = resultData[indexX][indexY];
                    indexY++;
                }
                indexX++;
                indexY = 0;
            }

            log.info(MSG_IMAGE_DATA_COLLECTED);

        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ServerConnectionTimeout("Could not connect to Server");
        }

    }

    String healthCheck(String ip, int port, int timeout) {

        String message = null;

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(ip, port), timeout);
            //Send the message to server
            DataOutputStream bw = new DataOutputStream(socket.getOutputStream());
            bw.write("ping\n".getBytes());
            bw.flush();

            log.info(HEATH_CHECK);

            //Get the return message from the server
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            message = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            inputStream.close();

        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return message;
    }

    private String createCommand() {
        StringBuilder cmd = new StringBuilder();
        cmd.append("GET /mandelbrot/")
                .append(minCRE)
                .append("/")
                .append(minCIM)
                .append("/")
                .append(maxCRE)
                .append("/")
                .append(maxCIM)
                .append("/")
                .append(width)
                .append("/")
                .append(height)
                .append("/")
                .append(startX)
                .append("/")
                .append(startY)
                .append("/")
                .append(maxIteration)
                .append("/")
                .append(division)
                .append("\n");
        return cmd.toString();
    }

    @SuppressWarnings({"squid:S00107", "squid:S00117"})
    void initParams(double min_c_re, double min_c_im, double max_c_re,
                    double max_c_im, int maxIteration, int width, int height, int division, int startX, int startY) {
        this.minCRE = min_c_re;
        this.minCIM = min_c_im;
        this.maxCRE = max_c_re;
        this.maxCIM = max_c_im;
        this.maxIteration = maxIteration;
        this.width = width;
        this.height = height;
        this.division = division;
        this.startX = startX;
        this.startY = startY;
    }
}
