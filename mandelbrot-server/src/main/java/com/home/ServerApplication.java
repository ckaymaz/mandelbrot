package com.home;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class ServerApplication
{
    public static void main(String[] args ) {
        Server server = new Server();

        int port = Integer.parseInt(args[0]);

        new Thread(() -> {
            try {
                server.processMandelbrotSet(port);
            } catch (IOException e) {
                log.info("Internal server error {} ", e.getMessage());
            }
        }).start();
    }
}