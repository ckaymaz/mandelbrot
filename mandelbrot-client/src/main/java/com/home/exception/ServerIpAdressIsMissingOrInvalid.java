package com.home.exception;

public class ServerIpAdressIsMissingOrInvalid extends Exception {

    public ServerIpAdressIsMissingOrInvalid(String message) {
        super(message);
    }
}
