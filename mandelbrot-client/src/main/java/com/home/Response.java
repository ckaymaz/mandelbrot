package com.home;

class Response {

    private int[][] imageData;

    Response(int[][] imgData) {
        this.imageData = imgData;
    }

    synchronized int[][] getImageData() {
        return imageData;
    }

}

