package com.home.exception;

public class InvalidNumberException extends Exception {

    public InvalidNumberException(String message) {
        super(message);
    }
}
