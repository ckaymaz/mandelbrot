package com.home.exception;

public class InvalidDivisorException extends Exception {

    public InvalidDivisorException(String message) {
        super(message);
    }
}
